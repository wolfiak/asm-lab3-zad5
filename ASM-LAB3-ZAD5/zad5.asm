.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.DATA
		
		cout		   dd ?
		cin			   dd ?

		source         dd 10 DUP(?)
		bufor		   dd 10 DUP(?)
		bufor2		   dd 10 DUP(?)
		tekst          db "Wprowadz liczbe %i :",0
		rozmiar        dd $ - tekst
		tekst2         db "Twoj wynik to: %i ",0
		rozmiar2        dd $ - tekst2
		liczba		   dd ?
		lznakow        dd 0
		lznakowz        dd 0
		wy             dd ?
		wynik          db 0
		znacznik       db 0
		tmp            db ?
		wmno           db ?
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	mov EAX, 1
	mov liczba, EAX

	mov ECX, 10
	mov EDI, OFFSET source

	Pentla:
	push ECX
	push liczba
	push EDI
	
	invoke wsprintfA, OFFSET bufor, OFFSET tekst, liczba

	invoke WriteConsoleA, cout, OFFSET bufor, rozmiar, OFFSET lznakow,0
    invoke ReadConsoleA, cin, OFFSET wy, 4, OFFSET lznakowz, 0
	
	mov EAX, wy
	pop EDI
	stosd

	pop liczba
	inc liczba
	pop ECX

    dec ECX
	jnz Pentla
	mov ECX, 10
	mov ESI, OFFSET source
	wys:
	push ECX
	mov EAX, 0
	lodsd
	mov wy, EAX
	cmp AH , 0dh
	je ndziesietnie
	mov znacznik, 1
	mov tmp, AH
	jmp ndziesietnie

	powrot:
	cmp znacznik, 1
	je dziesietnie
	tutaj:
	add AL, wynik
	mov wynik, AL
	invoke WriteConsoleA, cout, OFFSET wy, 4, OFFSET lznakow, 0


	pop ECX
	dec ECX
	jnz wys
	invoke wsprintfA, OFFSET bufor2, OFFSET tekst2, wynik
	invoke WriteConsoleA, cout, OFFSET bufor2, rozmiar2, OFFSET lznakow, 0
	 
	
		invoke ExitProcess, 0
	jeden:
	mov al,1d
	jmp powrot
	dwa:
	mov al,2d
	jmp powrot
	trzy:
	mov al,3d
	jmp powrot
	cztery:
	mov al,4d
	jmp powrot
	piec:
	mov al,5d
	jmp powrot
	szesc:
	mov al,6d
	jmp powrot
	siedem:
	mov al,7d
	jmp powrot
	osiem:
	mov al,8d
	jmp powrot
	dziewiec:
	mov al,9d
	jmp powrot
	ndziesietnie:
	cmp AL , 31h
	je jeden
	cmp AL , 32h
	je dwa
	cmp AL , 33h
	je trzy
	cmp AL , 34h
	je cztery
	cmp AL , 35h
	je piec
	cmp AL , 36h
	je szesc
	cmp AL , 37h
	je siedem
	cmp AL , 38h
	je osiem
	cmp AL , 39h
	je dziewiec
	jmp powrot

	dziesietnie:
	imul ax, 10d
	mov wmno, al
	mov AL, tmp

	cmp AL , 31h
	je jeden1
	cmp AL , 32h
	je dwa1
	cmp AL , 33h
	je trzy1
	cmp AL , 34h
	je cztery1
	cmp AL , 35h
	je piec1
	cmp AL , 36h
	je szesc1
	cmp AL , 37h
	je siedem1
	cmp AL , 38h
	je osiem1
	cmp AL , 39h
	je dziewiec1
	dodaj:
	add al, wmno
	


	jmp tutaj

	jeden1:
	mov al,1d
	jmp dodaj
	dwa1:
	mov al,2d
	jmp dodaj
	trzy1:
	mov al,3d
	jmp dodaj
	cztery1:
	mov al,4d
	jmp dodaj
	piec1:
	mov al,5d
	jmp dodaj
	szesc1:
	mov al,6d
	jmp dodaj
	siedem1:
	mov al,7d
	jmp dodaj
	osiem1:
	mov al,8d
	jmp dodaj
	dziewiec1:
	mov al,9d
	jmp dodaj

main endp

END